﻿Imports System.Data.SqlClient
Public Class Form2

    Private Sub Form2_Load(ByVal sender As Object, _
     ByVal e As EventArgs) Handles MyBase.Load

        Form1.GetGoalValues()

    End Sub

    Private Sub TextBox1_TextChanged(sender As Object, e As EventArgs) Handles TextBox1.TextChanged
        Dim Value As Decimal
        If Not Decimal.TryParse(TextBox1.Text, Value) Then Value = 0 : GoTo ShowMessage
        If Value > 100 Then Value = 100 : GoTo ShowMessage
        If Value < 0 Then Value = 0 : GoTo ShowMessage

        Exit Sub
ShowMessage:
        TextBox1.Text = Value
        MessageBox.Show("Der Wert war nicht in Reichweite!(0-100)", "Eingabewert angepasst!", MessageBoxButtons.OK, MessageBoxIcon.Warning)
    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        Form1.SaveGoalValues()
        Me.Close()
        Form1.Form2InFront = False
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Me.Close()
        Form1.Form2InFront = False
    End Sub

End Class