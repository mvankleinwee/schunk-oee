﻿Imports System.Data.SqlClient
Imports System.Configuration
Imports Opc.Ua
Imports Opc.Ua.Client
Imports Siemens.OpcUA
Imports System.Windows.Forms.DataVisualization.Charting
Imports System.Runtime.InteropServices
Imports Microsoft.Office.Interop
Imports Scripting

Public Class Form1
    Implements IMessageFilter

#Region "Fields"
    Dim DBConnection As New SqlConnection(ConfigurationManager.ConnectionStrings("CellroData").ConnectionString)
    Private WithEvents myTimer As New System.Windows.Forms.Timer()
    Dim FormLoaded As Boolean
    Dim FirstTickMonday As Boolean
    Dim oExcelApp As Excel.Application ''= New Excel.Application 'When excelapplication is initiated here then we get lots of problems with usage excel for user. Process is the nalways running. Now we create and release an process everytime.
    Dim oExcelWorkBook As Excel.Workbook
    Dim oWorkSheet As Excel.Worksheet
    Dim TabChangeCount As Integer
    Private m_Server As Server
    Dim m_NameSpaceIndex As UInt16 = 0
    Dim ConnectedState As Boolean
    Dim Machine_Run As Boolean
    Dim PLC_Alive_Bit As Boolean
    Dim Before_PLC_Alive_Bit As Boolean
    Dim RBT2_Run As Boolean
    Dim TimerInterval As Integer = 10000
    Dim IdleOnInputCount As Integer
    Dim Mylog As New FileSystemObject
    Dim Createlog As TextStream
    Public Form2InFront As Boolean
    Dim DowntimeIDs(3) As Integer
    ''Variables that are saved in Database
    Dim ProgVariables(15) As String
    'Dim ServerURL As String
    'Dim NamespaceURI As String
    'Dim LogFileLocation As String
    'Dim NameStringOutgoing As String
    'Dim NameStringInComing As String
    'Dim IN_IPC_Alive_Bit As String
    'Dim IN_Machine_Run As String
    'Dim OUT_IPC_Alive_Bit As String
    'Dim StartVNC As String
    'Dim CloseVNC As String
    'Dim DiagramPathName As String
    'Dim DiagramlFileExtension As String
    'Dim CauseRangeToRead As String
    'Dim DayRangeToRead As String
#End Region

    Private Declare Function FindWindow Lib "user32.dll" Alias "FindWindowA" (ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    Private Declare Function ShowWindow Lib "user32" (ByVal hwnd As IntPtr, ByVal nCmdShow As Integer) As Integer
    Private Const SW_SHOWNORMAL As Integer = 1
    Private Const SW_SHOWMINIMIZED As Integer = 2
    'ProgVariables Array Declared names<>Numbers
    Private Const ServerURL As Integer = 0
    Private Const NamespaceURI As Integer = 1
    Private Const LogFileLocation As Integer = 2
    Private Const NameStringOutgoing As Integer = 3
    Private Const NameStringInComing As Integer = 4
    Private Const IN_IPC_Alive_Bit As Integer = 5
    Private Const IN_Machine_Run As Integer = 6
    Private Const OUT_IPC_Alive_Bit As Integer = 7
    Private Const StartVNC As Integer = 8
    Private Const CloseVNC As Integer = 9
    Private Const DiagramPathName As Integer = 10
    Private Const DiagramlFileExtension As Integer = 11
    Private Const CauseRangeToRead As Integer = 12
    Private Const DayRangeToRead As Integer = 13
    Private Const IN_RBT2_Run As Integer = 14
    Private Const VNCWindowName As Integer = 15





    Private Sub Timer_Setup()
        myTimer.Interval = TimerInterval
        myTimer.Start()

    End Sub

    Private Sub TimerEventProcessor(myObject As Object, _
                                           ByVal myEventArgs As EventArgs) _
                                       Handles myTimer.Tick
        'Stop timer
        myTimer.Stop()

        'Keep track of IdleCount (For Tabcontrol.SelectedIndex = 2
        IdleOnInputCount = IdleOnInputCount + 1
        'Change tab number or minimize/maximizemin
        TabChangeCount = TabChangeCount + 1
        If TabControl1.SelectedIndex = 3 And IdleOnInputCount >= 7 Then     'To Reset to Main1 if Main3 is kept open
            TabControl1.SelectedIndex = 0
            IdleOnInputCount = 0
        ElseIf TabControl1.SelectedIndex <> 3 And TabChangeCount >= 1 Then
            If Form2InFront = False Then
                ChangeTab()
                IdleOnInputCount = 0
            End If
        Else
            'Do Nothing
        End If

        'Check if its first of january
        Dim Day1 As New DateTime(DateTime.Now.Year, 1, 1)
        If Date.Now = Day1 Then
            NewYear()
        End If

        'Check Monday FirstTick
        If Weekday(Now(), FirstDayOfWeek.Monday) = 1 Then       ''And FirstTickMonday = 0 
            If FirstTickMonday = 0 Then
                FirstTickMonday = 1
                WriteToLog("TimerEventProcessor: FirstTickMonday set to 1, NewWeek is called")
            End If
            NewWeek()           'Inside this function Already takes on account a check if active week is already put in database
        Else
            If FirstTickMonday = 1 Then
                FirstTickMonday = 0
                WriteToLog("TimerEventProcessor: FirstTickMonday set to 0")
            End If
        End If

        'Evaluate PLC values (If connected)
        If ConnectedState = True Then
            ReadPLCValues()
            HandleAliveBit()
            OvalShape2.FillColor = Color.Lime
            BtnConnect.Visible = False
        Else
            PLC_Alive_Bit = False
            OvalShape1.FillColor = Color.Red
            Machine_Run = False
            OvalShape2.FillColor = Color.Red
            RBT2_Run = False
            BtnConnect.Visible = True
            'Try to connect Again????? Or else what to do? Manually connect?
        End If

        LogMachState()       'Take New Day/Week also on account



        'Update chart (Auslastung)
        ChartUpdate("ActiveWeek")
        ChartUpdate("DowntimesActive")

        'Start timer again
        myTimer.Start()

    End Sub

    Public Function PreFilterMessage(ByRef m As Message) As Boolean Implements IMessageFilter.PreFilterMessage
        '' Retrigger timer on keyboard and mouse messages
        If (m.Msg >= &H100 And m.Msg <= &H109) Or (m.Msg >= &H200 And m.Msg <= &H20E) Then
            IdleOnInputCount = 0
        End If
    End Function

    Private Sub WriteToLog(WriteText As String)
        Dim LogFile As String = ProgVariables(LogFileLocation) & Date.Today & "mylog.txt"

        'Check if file for current day exists
        If Mylog.FileExists(LogFile) = False Then
            Createlog = Mylog.CreateTextFile(LogFile, False)
        Else
            Createlog = Mylog.OpenTextFile(LogFile, IOMode.ForAppending, False, Tristate.TristateUseDefault)
        End If

        Createlog.WriteLine(Date.Now & ": " & WriteText)
        Createlog.Close()
    End Sub

    Private Sub GetVariables()
        'To at least have default logfiles location (when database connection doesn't work)
        ProgVariables(LogFileLocation) = "D:\Cellro\Logfiles\"

        'Variables which can change over time are stored in table ProgVariables. So programmer from Cellro can adjust the fields in the table.
        Dim queryString As String = "Select * from dbo.ProgVariables"
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
        Dim dataSet As DataSet = New DataSet()

        adapter.Fill(dataSet, "ProgVariables")
        Dim i As Integer = 0
        Dim pRow As DataRow
        For Each pRow In dataSet.Tables("ProgVariables").Rows
            ProgVariables(i) = pRow("Value")
            i = i + 1
        Next


    End Sub

#Region "PLCCommunication"

    Private Sub m_Server_CertificateEvent(ByVal validator As CertificateValidator, ByVal e As CertificateValidationEventArgs)
        ' Accept all certificate -> better ask user
        e.Accept = True
    End Sub

    Private Function ConnectToPLC()

        ''Connect to server
        Try
            '' Connect with URL from Server
            m_Server.Connect(ProgVariables(ServerURL))
        Catch ex As Exception
            'Send to logfile
            'MessageBox.Show("Connect failed:" + ex.Message)
            WriteToLog("ConnectToPLC: Connect failed:" + ex.Message)
            Return False
        End Try

        ' Read Namespace Table
        Try
            Dim nodesToRead As NodeIdCollection = New NodeIdCollection
            Dim results As DataValueCollection
            nodesToRead.Add(Variables.Server_NamespaceArray)
            ' Read the namespace array
            m_Server.ReadValues(nodesToRead, results)
            If ((results.Count <> 1) _
                        OrElse (results(0).Value.GetType <> GetType(System.String()))) Then
                Throw New Exception("Reading namespace table returned unexptected result")
            End If

            ' Try to find the namespace URI entered by the user
            Dim nameSpaceArray() As String = CType(results(0).Value, String())
            Dim i As System.UInt16
            i = 0
            Do While (i < nameSpaceArray.Length)
                If (nameSpaceArray(i) = ProgVariables(NamespaceURI)) Then
                    m_NameSpaceIndex = i
                End If

                i = (i + 1)
            Loop

            ' Check if the namespace was found
            If (m_NameSpaceIndex = 0) Then
                Throw New Exception(("Namespace " _
                                + (ProgVariables(NamespaceURI) + " not found in server namespace table")))
            End If

        Catch ex As Exception
            'send to log file
            'MessageBox.Show(("Reading namespace table failed:" & ex.Message))
            WriteToLog("ConnectToPLC: Reading namespace table failed:" & ex.Message)
            Return False
        End Try

        Return True

    End Function

    Private Sub ReadPLCValues()
        Try

            Dim nodesToRead As NodeIdCollection = New NodeIdCollection
            Dim results As DataValueCollection
            Dim NameString As String = ProgVariables(NameStringOutgoing)

            nodesToRead.Add(New NodeId((NameString + ProgVariables(IN_IPC_Alive_Bit)), m_NameSpaceIndex))
            nodesToRead.Add(New NodeId((NameString + ProgVariables(IN_Machine_Run)), m_NameSpaceIndex))
            nodesToRead.Add(New NodeId((NameString + ProgVariables(IN_RBT2_Run)), m_NameSpaceIndex))

            m_Server.ReadValues(nodesToRead, results)

            If results.Count <> 3 Then
                'MsgBox("Reading value returned unexptected number of result")
                'Send to logfile
                WriteToLog("ReadPLCValues: Reading value returned unexptected number of result")
            End If

            PLC_Alive_Bit = results(0).Value
            Machine_Run = results(1).Value
            RBT2_Run = results(2).Value

            If StatusCode.IsBad(results(0).StatusCode) Then
                OvalShape1.BackColor = Color.WhiteSmoke
            Else
                'nothing
            End If

            If StatusCode.IsBad(results(1).StatusCode) Then
                OvalShape1.BackColor = Color.WhiteSmoke
            Else
                If Machine_Run = True Then
                    OvalShape1.FillColor = Color.Lime

                ElseIf Machine_Run = False Then
                    OvalShape1.FillColor = Color.Red

                End If
            End If

        Catch ex As Exception
            'Send to flogFile
            WriteToLog("ReadPLCValues: ERROR: " & ex.Message & "")
            ConnectedState = False
        End Try

    End Sub

    Private Sub WritePLCValue(nodeToWrite As String, valueToWrite As String, currentValue As Object)
        Try
            Dim NameString As String = ProgVariables(NameStringInComing)
            Dim nodesToWrite As NodeIdCollection = New NodeIdCollection
            Dim values As DataValueCollection = New DataValueCollection
            Dim results As StatusCodeCollection
            Dim value As [Variant] = New [Variant](Convert.ChangeType(valueToWrite, currentValue.GetType))
            nodesToWrite.Add(New NodeId((NameString + nodeToWrite), m_NameSpaceIndex))

            values.Add(New DataValue(value))
            m_Server.WriteValues(nodesToWrite, values, results)
            If StatusCode.IsBad(results(0)) Then
                Throw New Exception(StatusCode.LookupSymbolicId(results(0).Code))
            End If

        Catch ex As Exception
            'Send error to logfile
            WriteToLog("WritePLCValue: ERROR: " & ex.Message & "")
            'Something went wrong in writing the data (connection not OK????)
            ConnectedState = False
        End Try
    End Sub


#End Region


    Private Sub LogMachState()
        'Up and downtimes are logged in Database

        'Check if downtime log is active
        Dim queryString1 As String = "Select * from dbo.Downtimes Where StartTime is not NULL and EndTime is NULL"
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString1, DBConnection)
        Dim dataSet As DataSet = New DataSet()
        Dim DowntimeActive As Boolean
        Dim DowntimeDate As Date
        Dim DowntimeRBT2Run As Boolean
        Dim DowntimeID As Integer
        Dim today As Date = Date.Today
        Dim WeekNumber As Integer = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)

        adapter.Fill(dataSet, "Downtimes")
        If dataSet.Tables("Downtimes").Rows.Count <> 0 Then
            'Downtime log is active
            DowntimeActive = True
            DowntimeID = dataSet.Tables("Downtimes").Rows(0).Item("ID")
            'Set Downtimedate to seperate log into days
            DowntimeDate = dataSet.Tables("Downtimes").Rows(0).Item("Date")
            'To check if RBT2Run is active log
            If Not IsDBNull(dataSet.Tables("Downtimes").Rows(0).Item("Cause")) Then     'Text from DowntimesVariables table?????
                If dataSet.Tables("Downtimes").Rows(0).Item("Cause") = "WechselZeit Automation" Then
                    'Downtime RBT2Run active
                    DowntimeRBT2Run = True
                Else
                    DowntimeRBT2Run = False
                End If
            Else
                DowntimeRBT2Run = False
            End If
        Else
            DowntimeActive = False
            'Set to false also just in case
            DowntimeRBT2Run = False
            'DowntimeDate = "00/00/0000"
        End If

        Dim SQLCMD As New SqlCommand
        Dim queryString As String
        'DBConnection.Open()
        SQLCMD.Connection = DBConnection
        SQLCMD.CommandType = CommandType.Text
        'Machine_Run is always most important signal for logging! After that we check if downtime is active and if it is RBT2Run then we should log the minutes to Excel File
        If Machine_Run = True Then
            'If Downtime is active then End the active downtime log
            If DowntimeActive = True Then
                'Set endtime and assign Totaltime
                queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()) where StartTime is not NULL and EndTime is NULL"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()
                'When a downtime ends then if totaltimeminutes is < 10 minutes then delete it  
                queryString = "Delete From dbo.Downtimes Where TotalTimeMinutes < 10 And Type is null"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()
                'Check For activeRBT2Run This should be writen to Excel directly
                If DowntimeRBT2Run = True Then
                    'Then wright value to ExcelFile
                    WriteRBT2RunToExcelFile(DowntimeID)
                Else
                    'Do nothing
                End If
            End If
            'Add TimerInterval in seconds to Active Week Table And adjust the utilization
            Dim TimeToAdd As Integer = TimerInterval / 1000
            queryString = "Update dbo.ActiveWeek Set TimeInSeconds = (TimeInSeconds + " & TimeToAdd & "), Utilization = (TimeInSeconds/864) Where Date = CONVERT(VARCHAR(10),getdate(),103)"     '864 is (seconds per 24 hour divided bij 100), to determine the Utilization (Production per 24 hours)
            SQLCMD.CommandText = queryString
            SQLCMD.ExecuteNonQuery()
        Else
            'Log Downtime
            If DowntimeActive = True Then
                'Check if 'WechselZeit' is still active 
                If DowntimeRBT2Run = True Then
                    'Active Downtime was RBT2Run log, So Whenever a new log is made, the old active log should be writen to excel file.
                    If RBT2_Run = True Then
                        'Check if new day then close active one and create new record
                        Dim DaysDiff As Integer = DateDiff(DateInterval.Day, DowntimeDate, today)
                        If DaysDiff = 1 Then 'If it only is 1 day off
                            'Close active log
                            queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()) where StartTime is not NULL and EndTime is NULL"
                            SQLCMD.CommandText = queryString
                            SQLCMD.ExecuteNonQuery()
                            'Wright value to ExcelFile
                            WriteRBT2RunToExcelFile(DowntimeID)
                            'Enter a new log to the downtime table
                            queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined, Type, Cause) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & WeekNumber & ", 'False', 'Organisatorische Ausfalzeit To', 'WechselZeit Automation')"
                            SQLCMD.CommandText = queryString
                            SQLCMD.ExecuteNonQuery()
                        ElseIf DaysDiff > 1 Then
                            'More then 1 day difference, don't know what happened. Should not happen
                            WriteToLog("LogMachState: Downtime Active while that shouldn't be possible. Put to Undifined")
                            queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()), Undifined = 'True' where StartTime is not NULL and EndTime is NULL"
                            SQLCMD.CommandText = queryString
                            SQLCMD.ExecuteNonQuery()
                        Else
                            'Do nothing still current day
                        End If
                    Else
                        'Need to close active DowntimeRBT2Run log and create new one(undifined), RBT2Run not active anymore
                        'Close active log
                        queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()) where StartTime is not NULL and EndTime is NULL"
                        SQLCMD.CommandText = queryString
                        SQLCMD.ExecuteNonQuery()
                        'Wright value to ExcelFile
                        WriteRBT2RunToExcelFile(DowntimeID)
                        'Enter a new log to the downtime table
                        queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & WeekNumber & ", 'False')"
                        SQLCMD.CommandText = queryString
                        SQLCMD.ExecuteNonQuery()
                    End If

                Else
                    'Check if new day then close active one and create new record
                    Dim DaysDiff As Integer = DateDiff(DateInterval.Day, DowntimeDate, today)
                    If DaysDiff = 1 Then 'If it only is 1 day off
                        WriteToLog("LogMachState: New day started, create new LogID")
                        'Close active log
                        queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()) where StartTime is not NULL and EndTime is NULL"
                        SQLCMD.CommandText = queryString
                        SQLCMD.ExecuteNonQuery()
                        'Enter a new log to the downtime table
                        queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & WeekNumber & ", 'False')"
                        SQLCMD.CommandText = queryString
                        SQLCMD.ExecuteNonQuery()
                    ElseIf DaysDiff > 1 Then
                        'More then 1 day difference, don't know what happened. Should not happen
                        WriteToLog("LogMachState: Downtime Active while that shouldn't be possible. Put to Undifined")
                        queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()), Undifined = 'True' where StartTime is not NULL and EndTime is NULL"
                        SQLCMD.CommandText = queryString
                        SQLCMD.ExecuteNonQuery()
                    Else
                        'Do nothing still current day
                    End If
                End If
            Else
                If RBT2_Run = True Then
                    'Log downtime with 'WechselZeit' cause, RBT2Run is active
                    queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined, Type, Cause) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & WeekNumber & ", 'False', 'Organisatorische Ausfalzeit To', 'WechselZeit Automation')"
                    SQLCMD.CommandText = queryString
                    SQLCMD.ExecuteNonQuery()
                Else
                    'Enter a new log to the downtime table
                    queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & WeekNumber & ", 'False')"
                    SQLCMD.CommandText = queryString
                    SQLCMD.ExecuteNonQuery()
                End If

            End If
        End If


        'DBConnection.Close()

    End Sub

    Private Sub WriteRBT2RunToExcelFile(ID)
        'First Select TotalTimeMinutes from Downtimes table where = ID
        Dim queryString As String = "Select Date, TotalTimeMinutes, Cause From dbo.Downtimes Where ID = " & ID
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
        Dim dataSet As DataSet = New DataSet()

        adapter.Fill(dataSet, "Downtimes")
        If dataSet.Tables("Downtimes").Rows.Count <> 0 Then
            'Fill in ExcelFile
            Dim Column As Integer
            Dim RowNumber As Integer
            Dim Value As Integer
            Dim FileYear As Integer
            Dim TempDate As Date = dataSet.Tables("Downtimes").Rows(0).Item("Date")
            Dim CauseText As String = dataSet.Tables("Downtimes").Rows(0).Item("Cause")
            Dim MinutesToAdd As Integer = dataSet.Tables("Downtimes").Rows(0).Item("TotalTimeMinutes")
            Dim WeekNumber As Integer = DatePart("ww", TempDate, vbMonday, vbFirstFourDays)

            If WeekNumber = 1 Then
                FileYear = Year(TempDate.AddDays(7))
            ElseIf WeekNumber > 51 Then
                FileYear = Year(TempDate.AddDays(-GetMondayDiff()))
            Else
                FileYear = Year(TempDate)
            End If
            Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)         'Not Year from now, can also be week 53 with few days in next year???
            'Check if file exsists
            If Mylog.FileExists(FileName) = False Then
                'Send to logfile
                WriteToLog("WriteRBT2RunToExcelFile: " & FileName & " is not available")
                MsgBox(FileName & " nicht gefunden, Datei sollte verfügbar sein!", , "Fehler")
            Else
                'File is available go on
                'First check if file is opened, then close
                CheckExcelFileOpened()

                oExcelApp = New Excel.Application
                oExcelWorkBook = oExcelApp.Workbooks.Open(FileName)    'set year variable otherwise it will not work for upcoming years (combine weeknumber with year) 

                oWorkSheet = oExcelWorkBook.Worksheets("KW" & WeekNumber)
                'Find correct field (Row and Col)
                RowNumber = oWorkSheet.Range(ProgVariables(CauseRangeToRead)).Find(CauseText, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Row
                'First Pick right weekday and use this to select column
                Dim WeekdayNr As Integer = Weekday(TempDate, FirstDayOfWeek.Monday)
                Dim GermanDay As String
                Select Case WeekdayNr
                    Case 0  'Sunday
                        GermanDay = "So"
                    Case 1  'Monday
                        GermanDay = "Mo"
                    Case 2  'Tuesday
                        GermanDay = "Di"
                    Case 3  'Wednesday
                        GermanDay = "Mi"
                    Case 4  'Thursday
                        GermanDay = "Do"
                    Case 5  'Friday
                        GermanDay = "Fr"
                    Case 6  'Saturday
                        GermanDay = "Sa"
                End Select
                Column = oWorkSheet.Range(ProgVariables(DayRangeToRead)).Find(GermanDay, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Column
                'MsgBox(Column)
                Value = oWorkSheet.Cells(RowNumber, Column).Value
                'MsgBox(Value)
                oWorkSheet.Cells(RowNumber, Column) = Value + MinutesToAdd

                If oExcelWorkBook.Saved = False Then
                    oExcelWorkBook.Save()
                End If
                oExcelWorkBook.Close()
                oExcelApp.Quit()
                Marshal.ReleaseComObject(oWorkSheet)
                Marshal.ReleaseComObject(oExcelWorkBook)
                Marshal.ReleaseComObject(oExcelApp)
            End If
        End If

    End Sub

    Private Sub HandleAliveBit()
        'First check if signal is changed
        If Before_PLC_Alive_Bit <> PLC_Alive_Bit Then
            If PLC_Alive_Bit = True Then
                WritePLCValue(ProgVariables(OUT_IPC_Alive_Bit), "False", PLC_Alive_Bit)
            ElseIf PLC_Alive_Bit = False Then
                WritePLCValue(ProgVariables(OUT_IPC_Alive_Bit), "True", PLC_Alive_Bit)
            End If
            'To check if value is changed, If not then connection is down
            Before_PLC_Alive_Bit = PLC_Alive_Bit
        Else
            WriteToLog("HandleAliveBit: PLC_Alive_Bit was same as before, Value = " & Before_PLC_Alive_Bit)
            'Connection is down???
            ConnectedState = False
        End If


    End Sub

    Private Sub ChangeTab()
        'Sub for changing selected Tab (only if first 2 tabs are in front)
        '0 = Auslastung MC1, 1 = Rückblick Auslastung MC1, 2 = VNC Viewer, 3 = Enter downtimes
        If TabControl1.SelectedIndex = 0 Then
            TabControl1.SelectedIndex = 1
        ElseIf TabControl1.SelectedIndex = 1 Then
            TabControl1.SelectedIndex = 2
            TabChangeCount = 0
        ElseIf TabControl1.SelectedIndex = 2 Then
            TabControl1.SelectedIndex = 0
            TabChangeCount = 0
        End If
    End Sub

    Private Sub NewWeek()
        'first check if current week is not already in ActiveWeek table
        Dim queryString1 As String = "Select * From dbo.Activeweek Where Date = CONVERT(VARCHAR(10),GETDATE(),103)"
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString1, DBConnection)
        Dim dataSet As DataSet = New DataSet()
        Dim WeekNumber As Integer = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)

        adapter.Fill(dataSet, "ActiveWeek")
        If dataSet.Tables("ActiveWeek").Rows.Count <> 0 Then
            'Active week already in table, Do nothing
            Return
        End If

        'Step 1: Update PastWeeks table (Insert the data from active week, and delete older data then 3 weeks ago)
        Dim SQLCMD As New SqlCommand
        Dim queryString2 As String
        'DBConnection.Open()
        SQLCMD.Connection = DBConnection
        SQLCMD.CommandType = CommandType.Text
        'Make room for new rows
        queryString2 = "Update PastWeeks Set Id = Id + 7"
        SQLCMD.CommandText = queryString2
        SQLCMD.ExecuteNonQuery()
        'Inserting data from table activeweek
        queryString2 = "Insert into PastWeeks (Id, [Day], [Date], Utilization, Availability, WeekNumber) Select Id, [Day], [Date], Utilization, Availability, WeekNumber from ActiveWeek"
        SQLCMD.CommandText = queryString2
        SQLCMD.ExecuteNonQuery()
        'Update to put the avarage values
        queryString2 = "Update dbo.PastWeeks set [Average%] = (Select Left(AVG(Utilization),5) from dbo.ActiveWeek), [GoalAverage%] = (Select left(AVG(Goal),5) from dbo.ActiveWeek) Where Id < 8"
        SQLCMD.CommandText = queryString2
        SQLCMD.ExecuteNonQuery()
        'Delete Older data then 3 weeks ago (Current weeknumber - 3)
        ''''''''queryString2 = "Delete FROM PastWeeks WHERE convert(datetime, [Date], 103) < DATEADD(DAY,-21,GETDATE())"
        Dim BeforeDateDelete As Date
        Dim MondayOfWeek As Date
        MondayOfWeek = Date.Today.AddDays(-GetMondayDiff())
        BeforeDateDelete = MondayOfWeek.AddDays(-21)
        queryString2 = "Delete From dbo.PastWeeks Where convert(Datetime, Date, 103) < convert(Datetime, '" & BeforeDateDelete & "', 103)"
        SQLCMD.CommandText = queryString2
        SQLCMD.ExecuteNonQuery()
        'Also delete Downtimes logs older then 3 weeks
        queryString2 = "Delete From dbo.Downtimes Where convert(Datetime, Date, 103) < convert(Datetime, '" & BeforeDateDelete & "', 103)"
        SQLCMD.CommandText = queryString2
        SQLCMD.ExecuteNonQuery()

        'Delete logs older then BeforeDateDelete
        Dim Directory As New IO.DirectoryInfo(ProgVariables(LogFileLocation))
        For Each File As IO.FileInfo In Directory.GetFiles
            If File.Extension.Equals(".txt") AndAlso (Now - File.CreationTime).Days > 21 Then
                File.Delete()
            End If
        Next

        'Step 2: Update Active week table (Overwrite the old data with new dates and zero data)
        'First decide monday's date
        For i As Integer = 0 To 6
            queryString2 = "Update dbo.ActiveWeek set [Date] = CONVERT(VARCHAR(10),DATEADD(DAY," & i - GetMondayDiff() & ", GETDATE()),103), TimeInSeconds = 0, Utilization = 0, Availability = 0, WeekNumber = " & WeekNumber & " Where Id = " & i + 1
            SQLCMD.CommandText = queryString2
            SQLCMD.ExecuteNonQuery()
        Next

        ChartUpdate("PastWeeks")

        'DBConnection.Close()

    End Sub

    Private Sub NewYear()
        'Check if file for current year exists, if not then create.
        Dim FileName As String = ProgVariables(DiagramPathName) & Year(Date.Now) & ProgVariables(DiagramlFileExtension)         'Not Year from now, can also be week 53 with few days in next year???

        'Check if file exsists
        If Mylog.FileExists(FileName) = True Then
            'Do nothing, already available
            WriteToLog("NewYear: " & FileName & " is already available")
        Else
            'Create File for current year
            FileCopy(ProgVariables(DiagramPathName) & ProgVariables(DiagramlFileExtension), FileName)
            WriteToLog("NewYear: " & FileName & " is Created")
        End If

    End Sub

    Public Function GetMondayDiff()
        'Determine the difference of current day with monday.
        Dim today As Date = Date.Today
        Dim dayIndex As Integer = today.DayOfWeek
        If dayIndex < DayOfWeek.Monday Then
            dayIndex += 7 'Monday is first day of week, no day of week should have a smaller index
        End If
        Dim dayDiff As Integer = dayIndex - DayOfWeek.Monday

        Return dayDiff
    End Function

    Private Sub Form1_Load(ByVal sender As Object, _
     ByVal e As System.EventArgs) Handles MyBase.Load
        'First fill variables
        GetVariables()

        WriteToLog("Form1_Load: trying to start Application!")
        System.Windows.Forms.Application.AddMessageFilter(Me)
        'Start VNCViewer
        'Process.Start(ProgVariables(StartVNC))
        'Open Databaseconnection
        DBConnection.Open()
        m_Server = New Server

        AddHandler m_Server.CertificateEvent, AddressOf m_Server_CertificateEvent

        'Create chart 1
        Chart1.Titles.Add("TitleChart1")
        Chart1.Titles("Title1").Text = "Auslastung MC1 KW"
        Chart1.Titles("Title1").Font = New System.Drawing.Font(Chart1.Titles("Title1").Font.FontFamily, 20, FontStyle.Bold)

        Chart1.ChartAreas.Add("Area")
        Chart1.ChartAreas("Area").AxisX.Minimum = 0
        Chart1.ChartAreas("Area").AxisX.Maximum = 8
        Chart1.ChartAreas("Area").AxisX.Interval = 1
        Chart1.ChartAreas("Area").AxisY.Title = "[%] Prozentsatz"
        Chart1.ChartAreas("Area").AxisY.TitleAlignment = StringAlignment.Center
        Chart1.ChartAreas("Area").AxisY.TitleFont = New System.Drawing.Font(Chart1.Titles("Title1").Font.FontFamily, 10, FontStyle.Regular)
        Chart1.ChartAreas("Area").AxisY.TextOrientation = TextOrientation.Rotated270
        Chart1.ChartAreas("Area").AxisY.Minimum = 0
        Chart1.ChartAreas("Area").AxisY.Maximum = 100
        Chart1.ChartAreas("Area").AxisY.Interval = 20

        Chart1.Series.Add("Auslastung MC1")
        Chart1.Series("Auslastung MC1").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column
        Chart1.Series("Auslastung MC1").Color = Color.FromArgb(0, 58, 105)
        Chart1.Series("Auslastung MC1")("PointWidth") = 0.7
        Chart1.Series.Add("Ziel")
        Chart1.Series("Ziel").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Chart1.Series("Ziel").BorderWidth = 2
        Chart1.Series("Ziel").MarkerStyle = DataVisualization.Charting.MarkerStyle.Square
        Chart1.Series("Ziel").MarkerSize = 10
        Chart1.Series("Ziel").Color = Color.Green
        Chart1.Series("Ziel").XValueMember = "Days"

        Chart1.Legends.Add("Auslastung MC1")
        Chart1.Legends.Add("Ziel")
        Chart1.Legends(0).BorderColor = Color.Black
        Chart1.Legends(0).Docking = Docking.Bottom
        Chart1.Legends(0).TableStyle = LegendTableStyle.Tall
        Chart1.Legends(0).IsDockedInsideChartArea = True
        Chart1.Legends(0).Alignment = StringAlignment.Near

        'Create chart 3
        Chart3.Titles.Add("TitleChart3")
        Chart3.Titles("Title1").Text = "StillstandsGründe MC1 KW"
        Chart3.Titles("Title1").Font = New System.Drawing.Font(Chart1.Titles("Title1").Font.FontFamily, 20, FontStyle.Bold)
        Chart3.ChartAreas.Add("Area")
        Chart3.ChartAreas("Area").AxisX.Minimum = 0               'this needs to be generated automatically due to available data
        Chart3.ChartAreas("Area").AxisX.Maximum = 11
        Chart3.ChartAreas("Area").AxisX.Interval = 1
        Chart3.ChartAreas("Area").AxisY.Title = "Stunde [h]"
        Chart3.ChartAreas("Area").AxisY.TitleAlignment = StringAlignment.Center
        Chart3.ChartAreas("Area").AxisY.TitleFont = New System.Drawing.Font(Chart3.Titles("Title1").Font.FontFamily, 10, FontStyle.Regular)
        Chart3.ChartAreas("Area").AxisY.TextOrientation = TextOrientation.Rotated270
        Chart3.ChartAreas("Area").AxisY.Minimum = 0
        Chart3.ChartAreas("Area").AxisY.Maximum = 100
        Chart3.ChartAreas("Area").AxisY.Interval = 20

        Chart3.Series.Add("StillstandsGründe MC1")
        Chart3.Series("StillstandsGründe MC1").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column
        Chart3.Series("StillstandsGründe MC1").Color = Color.FromArgb(0, 58, 105)
        Chart3.Series("StillstandsGründe MC1")("PointWidth") = 0.7

        'Chart3.Legends.Add("StillstandsGründe MC1")
        'Chart3.Legends(0).BorderColor = Color.Black
        'Chart3.Legends(0).Docking = Docking.Bottom
        'Chart3.Legends(0).TableStyle = LegendTableStyle.Tall
        'Chart3.Legends(0).IsDockedInsideChartArea = True
        'Chart3.Legends(0).Alignment = StringAlignment.Near

        'Create chart 2
        Chart2.Titles.Add("TitleChart2")
        Chart2.Titles("Title1").Text = "Rückblick Auslastung MC1 KW"
        Chart2.Titles("Title1").Font = New System.Drawing.Font(Chart2.Titles("Title1").Font.FontFamily, 20, FontStyle.Bold)
        Chart2.ChartAreas.Add("Area")
        Chart2.ChartAreas("Area").AxisX.Minimum = 0
        Chart2.ChartAreas("Area").AxisX.Maximum = 22
        Chart2.ChartAreas("Area").AxisX.Interval = 1
        Chart2.ChartAreas("Area").AxisY.Title = "Prozentsatz [%]"
        Chart2.ChartAreas("Area").AxisY.TitleAlignment = StringAlignment.Center
        Chart2.ChartAreas("Area").AxisY.TitleFont = New System.Drawing.Font(Chart2.Titles("Title1").Font.FontFamily, 10, FontStyle.Regular)
        Chart2.ChartAreas("Area").AxisY.TextOrientation = TextOrientation.Rotated270
        Chart2.ChartAreas("Area").AxisY.Minimum = 0
        Chart2.ChartAreas("Area").AxisY.Maximum = 100
        Chart2.ChartAreas("Area").AxisX.LabelStyle.Angle = -90
        'Chart2.ChartAreas("Area").AxisY.Interval = 10

        Chart2.Series.Add("Auslastung MC1")
        Chart2.Series("Auslastung MC1").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column
        Chart2.Series("Auslastung MC1").Color = Color.FromArgb(0, 58, 105)
        Chart2.Series("Auslastung MC1")("PointWidth") = 0.6
        Chart2.Series.Add("Ziel Durchschnitt[%]")
        Chart2.Series("Ziel Durchschnitt[%]").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Chart2.Series("Ziel Durchschnitt[%]").BorderWidth = 5
        Chart2.Series("Ziel Durchschnitt[%]").Color = Color.Green
        Chart2.Series.Add("Durchschnitt MC1[%]")
        Chart2.Series("Durchschnitt MC1[%]").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line
        Chart2.Series("Durchschnitt MC1[%]").BorderWidth = 5
        Chart2.Series("Durchschnitt MC1[%]").Color = Color.DarkGray

        Chart2.Legends.Add("Auslastung MC1")
        Chart2.Legends.Add("Ziel Durchschnitt[%]")
        Chart2.Legends(0).BorderColor = Color.Black
        Chart2.Legends(0).Docking = Docking.Bottom
        Chart2.Legends(0).TableStyle = LegendTableStyle.Tall
        Chart2.Legends(0).IsDockedInsideChartArea = True
        Chart2.Legends(0).Alignment = StringAlignment.Near

        'Create chart 4
        Chart4.Titles.Add("TitleChart4")
        Chart4.Titles("Title1").Text = "StillstandsGründe MC1 KW"
        Chart4.Titles("Title1").Font = New System.Drawing.Font(Chart1.Titles("Title1").Font.FontFamily, 20, FontStyle.Bold)
        Chart4.ChartAreas.Add("Area")
        Chart4.ChartAreas("Area").AxisX.Minimum = 0               'this needs to be generated automatically due to available data
        Chart4.ChartAreas("Area").AxisX.Maximum = 11
        Chart4.ChartAreas("Area").AxisX.Interval = 1
        Chart4.ChartAreas("Area").AxisY.Title = "Stunde [h]"
        Chart4.ChartAreas("Area").AxisY.TitleAlignment = StringAlignment.Center
        Chart4.ChartAreas("Area").AxisY.TitleFont = New System.Drawing.Font(Chart4.Titles("Title1").Font.FontFamily, 10, FontStyle.Regular)
        Chart4.ChartAreas("Area").AxisY.TextOrientation = TextOrientation.Rotated270
        Chart4.ChartAreas("Area").AxisY.Minimum = 0
        Chart4.ChartAreas("Area").AxisY.Maximum = 100
        Chart4.ChartAreas("Area").AxisY.Interval = 20

        Chart4.Series.Add("StillstandsGründe MC1")
        Chart4.Series("StillstandsGründe MC1").ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Column
        Chart4.Series("StillstandsGründe MC1").Color = Color.FromArgb(0, 58, 105)
        Chart4.Series("StillstandsGründe MC1")("PointWidth") = 0.7

        'Chart4.Legends.Add("StillstandsGründe MC1")
        'Chart4.Legends(0).BorderColor = Color.Black
        'Chart4.Legends(0).Docking = Docking.Bottom
        'Chart4.Legends(0).TableStyle = LegendTableStyle.Tall
        'Chart4.Legends(0).IsDockedInsideChartArea = True
        'Chart4.Legends(0).Alignment = StringAlignment.Near

        'Initialize startup
        Form2InFront = False
        UpdateActiveDowntime()
        NewYear()
        NewWeek()
        ChartUpdate("ActiveWeek")
        ChartUpdate("PastWeeks")
        ChartUpdate("DowntimesActive")

        If ConnectToPLC() = True Then
            ConnectedState = True
            WritePLCValue(ProgVariables(OUT_IPC_Alive_Bit), "True", PLC_Alive_Bit)
            WriteToLog("Form1_Load: Succesfully connected")
        Else
            OvalShape2.FillColor = Color.Red
            BtnConnect.Visible = True
        End If

        'To prevent VNC to be at start in front
        Dim iHwnd As IntPtr = FindWindow(vbNullString, ProgVariables(VNCWindowName))
        ShowWindow(iHwnd, SW_SHOWMINIMIZED)

        GetDataExcelFile()

        Timer_Setup()
        FormLoaded = 1
        WriteToLog("Form1_Load: Form is loaded!")
    End Sub

    Private Sub Form1_FormClosing(sender As Object, e As FormClosingEventArgs) _
     Handles MyBase.FormClosing
        Dim weeknumber As Integer = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)
        If MsgBox("Möchten Sie schließen", MessageBoxButtons.YesNo, "Anwendungsmeldung") = DialogResult.No Then
            e.Cancel = True
            WriteToLog("Form1_FormClosing: Cancelled")
        Else
            'Close Current logging state and start an undifined one???
            WriteToLog("Form1_FormClosing: Closing Application")
            'Stop timer and create downtime variable or close current one??------------------------------------------------------------------------------
            myTimer.Stop()

            Try
                Dim SQLCMD As New SqlCommand
                Dim queryString As String
                'DBConnection.Open()
                SQLCMD.Connection = DBConnection
                SQLCMD.CommandType = CommandType.Text
                queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()) where StartTime is not NULL and EndTime is NULL"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()
                queryString = "Insert into dbo.Downtimes (Date, StartTime, WeekNumber, Undifined) Values (CONVERT(VARCHAR(10),getdate(),103), getdate(), " & weeknumber & ", 'True')"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()

                DBConnection.Close()
            Catch ex As Exception
                WriteToLog("Form1_FormClosing: Error closing active log and/or DBconnection")
            End Try

            Try
                'Close VNCViewer as well
                Process.Start(ProgVariables(CloseVNC))
            Catch ex As Exception
                WriteToLog("Form1_FormClosing: Error closing VNCviewer")
            End Try
            WriteToLog("Form1_FormClosing: Application Closed")
        End If

    End Sub

    Private Sub Form1_SizeChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.SizeChanged
        Dim iHwnd As IntPtr = FindWindow(vbNullString, ProgVariables(VNCWindowName)) '"WinVNC viewonly"
        If Me.WindowState = FormWindowState.Minimized Then
            If iHwnd = 0 Then
                'VNC isn't Activated, Do nothing. Form is also minimized
            Else
                'Vnc is running, Minimize 
                ShowWindow(iHwnd, SW_SHOWMINIMIZED)
            End If
        End If
        If Me.WindowState = FormWindowState.Maximized Or Me.WindowState = FormWindowState.Normal Then
            If iHwnd = 0 And TabControl1.SelectedIndex = 2 Then
                'VNC Window does not exsists, can start again
                Process.Start(ProgVariables(StartVNC))
            Else
                'Vnc is running, Minimize or show screen
                If TabControl1.SelectedIndex = 2 Then
                    ShowWindow(iHwnd, SW_SHOWNORMAL)
                Else
                    ShowWindow(iHwnd, SW_SHOWMINIMIZED)
                End If
            End If
        End If
    End Sub

    Private Sub UpdateActiveDowntime()
        Dim SQLCMD As New SqlCommand
        Dim queryString As String
        'DBConnection.Open()
        SQLCMD.Connection = DBConnection
        SQLCMD.CommandType = CommandType.Text
        queryString = "Update dbo.Downtimes Set EndTime = GetDate(), TotalTimeMinutes = DateDiff(Mi,StartTime,GetDate()), Undifined = 'True' where StartTime is not NULL and EndTime is NULL"
        SQLCMD.CommandText = queryString
        SQLCMD.ExecuteNonQuery()
    End Sub

    Function WordWrap(ByVal Text As String, Optional ByVal MaxLineLen As Integer = 70)
        Dim i As Integer
        For i = 1 To Len(Text) / MaxLineLen
            Text = Mid(Text, 1, MaxLineLen * i - 1) & Replace(Text, " ", vbCrLf, MaxLineLen * i, 1, vbTextCompare)
        Next i
        WordWrap = Text
    End Function

    Private Sub ChartUpdate(ChartName)

        If ChartName = "ActiveWeek" Then
            Dim queryString As String = "Select Id ,Day, Date , Utilization, Goal, WeekNumber From dbo.Activeweek"
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
            Dim dataSet As DataSet = New DataSet()

            adapter.Fill(dataSet, "ActiveWeek")
            Chart1.Series("Auslastung MC1").Points.Clear()
            Chart1.Series("Ziel").Points.Clear()
            'Loop for getting all days of the week
            Dim pRow As DataRow
            For Each pRow In dataSet.Tables("ActiveWeek").Rows
                Chart1.Series("Auslastung MC1").Points.AddXY(pRow("Id"), pRow("Utilization"))
                Chart1.Series("Ziel").Points.AddXY(pRow("Id"), pRow("Goal"))
                Chart1.Series("Ziel").Points((pRow("Id") - 1)).AxisLabel = pRow("Date") & vbCrLf & pRow("Day")
            Next
            Chart1.Titles("Title1").Text = "Auslastung MC1 KW " & pRow("WeekNumber")
            Chart3.Titles("Title1").Text = "StillstandsGründe MC1 KW " & pRow("WeekNumber")
        ElseIf ChartName = "PastWeeks" Then
            Dim queryString As String = "Select Id, [Day], [Date], Utilization, [Average%], [GoalAverage%], WeekNumber From Pastweeks Order by convert(datetime, [Date], 103) asc"
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
            Dim dataSet As DataSet = New DataSet()

            adapter.Fill(dataSet, "PastWeeks")
            Chart2.Series("Auslastung MC1").Points.Clear()
            Chart2.Series("Durchschnitt MC1[%]").Points.Clear()
            Chart2.Series("Ziel Durchschnitt[%]").Points.Clear()
            'Loop for getting all days of the week
            Dim WeekNumbers As String = 0
            Dim FirstWeekNumber As Integer = 0
            Dim LastWeekNumber As Integer = 0
            Dim pRow As DataRow
            Dim i As Integer = 1
            For Each pRow In dataSet.Tables("PastWeeks").Rows
                If FirstWeekNumber = 0 Then
                    FirstWeekNumber = pRow("WeekNumber")
                ElseIf pRow("WeekNumber") <> LastWeekNumber Then
                    LastWeekNumber = pRow("WeekNumber")
                End If
                Chart2.Series("Auslastung MC1").Points.AddXY(i, pRow("Utilization"))
                Chart2.Series("Durchschnitt MC1[%]").Points.AddXY(i, pRow("Average%"))
                Chart2.Series("Ziel Durchschnitt[%]").Points.AddXY(i, pRow("GoalAverage%"))
                Chart2.Series("Ziel Durchschnitt[%]").Points((i - 1)).AxisLabel = pRow("Date") & vbCrLf & pRow("Day")
                i = i + 1
            Next
            Chart2.Titles("Title1").Text = "Rückblick Auslastung MC1 KW " & FirstWeekNumber & "-" & LastWeekNumber
            Chart4.Titles("Title1").Text = "Rückblick StillstandsGründe MC1 KW " & FirstWeekNumber & "-" & LastWeekNumber
        ElseIf ChartName = "DowntimesActive" Then
            'Both graphs to update (Active week and past weeks)
            'First Clear the values
            Chart3.Series("StillstandsGründe MC1").Points.Clear()
            Chart4.Series("StillstandsGründe MC1").Points.Clear()

            'Chart3.Series("StillstandsGründe MC1").Points.AddXY(11, 10)       'To show Empty Graph
            ''Chart3.Series("StillstandsGründe MC1").Points(10).AxisLabel = "1"
            'Chart3.ChartAreas("Area").AxisY.Maximum = 100
            'Chart3.ChartAreas("Area").AxisY.Interval = 20
            'Chart4.Series("StillstandsGründe MC1").Points.AddXY(11, 10)       'To show Empty Graph
            ''Chart4.Series("StillstandsGründe MC1").Points(10).AxisLabel = "1"
            'Chart4.ChartAreas("Area").AxisY.Maximum = 100
            'Chart4.ChartAreas("Area").AxisY.Interval = 20

            'First select Active week
            Dim ActiveWeekNumber As Integer
            ActiveWeekNumber = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)

            'Active week Downtimes graph
            Dim queryString As String = "Select Sum(TotalTimeMinutes) as TotalTime, Cause From dbo.Downtimes Where WeekNumber = " & ActiveWeekNumber & " And TotalTimeMinutes is not NULL And Cause is not NULL Group By Cause Order by TotalTime Desc"
            Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
            Dim dataSet As DataSet = New DataSet()

            adapter.Fill(dataSet, "Downtimes")
            'Loop for Filling all Causes up to 10 kinds
            Dim i As Integer = 1
            Dim Count As Integer = dataSet.Tables("Downtimes").Rows.Count
            Dim Maximum As Integer
            Dim SummOther As Decimal = 0
            Dim Hours As Decimal
            For Each pRow In dataSet.Tables("Downtimes").Rows
                If i = 1 Then
                    'Determine Maximun value to adjust de Y-Axis
                    Hours = pRow("TotalTime") / 60                              ''Math.Ceiling(pRow("TotalTime") / 60)
                    Maximum = Math.Ceiling(Hours / 10) * 10
                    Chart3.ChartAreas("Area").AxisY.Maximum = Maximum
                    Chart3.ChartAreas("Area").AxisY.Interval = (Maximum / 5)
                End If

                If Count > 10 And i > 9 Then
                    'Summs the rest of the fields to show up ass Other
                    SummOther = SummOther + pRow("TotalTime")
                    Hours = SummOther / 60                                       ''Math.Ceiling(SummOther / 60)
                    Chart3.Series("StillstandsGründe MC1").Points.AddXY(10, Hours)      '10 is last possible, always other
                    Chart3.Series("StillstandsGründe MC1").Points((i - 1)).AxisLabel = "Übrig"
                Else
                    Hours = pRow("TotalTime") / 60
                    Chart3.Series("StillstandsGründe MC1").Points.AddXY(i, Hours)
                    Chart3.Series("StillstandsGründe MC1").Points((i - 1)).AxisLabel = WordWrap(pRow("Cause"), 10)
                End If
                i = i + 1
            Next

            'Past weeks Downtimes Graph
            Dim queryString1 As String = "Select Sum(TotalTimeMinutes) as TotalTime, Cause From dbo.Downtimes Where WeekNumber != " & ActiveWeekNumber & " And TotalTimeMinutes is not NULL And Cause is not NULL Group By Cause Order by TotalTime Desc"
            Dim adapter1 As SqlDataAdapter = New SqlDataAdapter(queryString1, DBConnection)
            Dim dataSet1 As DataSet = New DataSet()

            adapter1.Fill(dataSet1, "Downtimes")
            'Loop for Filling all Causes up to 10 kinds
            i = 1
            Count = dataSet1.Tables("Downtimes").Rows.Count
            SummOther = 0

            For Each pRow In dataSet1.Tables("Downtimes").Rows
                If i = 1 Then
                    'Determine Maximun value to adjust de Y-Axis
                    Hours = pRow("TotalTime") / 60
                    Maximum = Math.Ceiling(Hours / 10) * 10
                    Chart4.ChartAreas("Area").AxisY.Maximum = Maximum
                    Chart4.ChartAreas("Area").AxisY.Interval = (Maximum / 5)
                End If

                If Count > 10 And i > 9 Then
                    'Summs the rest of the fields to show up ass Other
                    SummOther = SummOther + pRow("TotalTime")
                    Hours = SummOther / 60
                    Chart4.Series("StillstandsGründe MC1").Points.AddXY(10, Hours)      '10 is last possible, always other
                    Chart4.Series("StillstandsGründe MC1").Points((i - 1)).AxisLabel = "Übrig"
                Else
                    Hours = pRow("TotalTime") / 60
                    Chart4.Series("StillstandsGründe MC1").Points.AddXY(i, Hours)
                    Chart4.Series("StillstandsGründe MC1").Points((i - 1)).AxisLabel = WordWrap(pRow("Cause"), 10)
                End If
                i = i + 1
            Next

        End If


    End Sub

    Public Sub GetGoalValues()
        Dim queryString As String = "Select Day, Goal From dbo.Activeweek"
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
        Dim dataSet As DataSet = New DataSet()

        adapter.Fill(dataSet, "ActiveWeek")

        'Loop for getting all days of the week


        Dim i As Integer = 0
        Dim pRow As DataRow
        For Each pRow In dataSet.Tables("ActiveWeek").Rows
            i = i + 1
            Form2.Controls("TextBox" & i).Text = pRow("Goal")
        Next
    End Sub

    Public Sub SaveGoalValues()
        Dim SQLCMD As New SqlCommand
        Dim queryString As String
        'DBConnection.Open()
        SQLCMD.Connection = DBConnection
        SQLCMD.CommandType = CommandType.Text
        'Make room for new rows
        For i As Integer = 1 To 7
            queryString = "Update ActiveWeek Set Goal = " & Form2.Controls("TextBox" & i).Text & " Where ID = " & i
            SQLCMD.CommandText = queryString
            SQLCMD.ExecuteNonQuery()
        Next
        ChartUpdate("ActiveWeek")
        'DBConnection.Close()
    End Sub

    Public Sub FillTextBoxes()
        'Get Downtime logs without a cause and put them in the textboxes
        Dim FoundTextBox1() As Control
        Dim FoundTextBox2() As Control

        'First clear the textboxes and delete buttons enabled = false
        BtnDelete1.Enabled = False
        BtnDelete2.Enabled = False
        BtnDelete3.Enabled = False
        BtnDelete4.Enabled = False
        For x = 1 To 8 Step 2
            FoundTextBox1 = Me.Controls.Find(String.Format("TextBox" & x, x), True)
            FoundTextBox2 = Me.Controls.Find(String.Format("TextBox" & (x + 1), x), True)
            CType(FoundTextBox1(0), TextBox).Text = ""
            CType(FoundTextBox2(0), TextBox).Text = ""
            CType(FoundTextBox2(0), TextBox).Enabled = False
        Next

        Dim queryString As String = "Select ID, StartTime, TotalTimeMinutes from dbo.Downtimes where TotalTimeMinutes is not NULL and [Type] is null and Cause is null And Undifined = 'False'"
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
        Dim dataSet As DataSet = New DataSet()

        adapter.Fill(dataSet, "Downtimes")
        'Loop for getting first 4 records
        Dim i As Integer = 1
        Dim pRow As DataRow
        Dim RecordCount As Integer = 0
        For Each pRow In dataSet.Tables("Downtimes").Rows
            If i < 8 Then
                FoundTextBox1 = Me.Controls.Find(String.Format("TextBox" & i, i), True)
                FoundTextBox2 = Me.Controls.Find(String.Format("TextBox" & (i + 1), i), True)
                CType(FoundTextBox1(0), TextBox).Text = pRow("StartTime")
                If pRow("TotalTimeMinutes") = 0 Then
                    CType(FoundTextBox2(0), TextBox).Text = 1
                Else
                    CType(FoundTextBox2(0), TextBox).Text = pRow("TotalTimeMinutes")
                End If
                CType(FoundTextBox2(0), TextBox).Enabled = True
                i = i + 2   '+2 because textboxes are combined (1-2, 3-4, etc...)
                DowntimeIDs(RecordCount) = pRow("ID")
                RecordCount += 1
            Else
                'Do nothing, Can only show 4 at the time.
            End If
        Next
        Dim FoundButton() As Control
        For x = 0 To RecordCount
            If x > 0 And x < 5 Then
                FoundButton = Me.Controls.Find(String.Format("BtnDelete" & x, x), True)
                CType(FoundButton(0), Button).Enabled = True
            End If
        Next



    End Sub

    Public Sub FillComboboxes()
        'Fill combobox with available Downtime "Types"
        Dim queryString As String = "Select [Type] From dbo.DowntimeVariables Group by [Type] Order by MIN(Id) Asc "
        Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
        Dim dataSet As DataSet = New DataSet()

        Dim FoundComboBox1() As Control
        Dim FoundComboBox2() As Control
        Dim FoundTextBox() As Control
        adapter.Fill(dataSet, "DowntimeVariables")
        For i = 1 To 10 Step 2
            FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox" & i, i), True)
            FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox" & (i + 1), i), True)
            CType(FoundComboBox1(0), ComboBox).Items.Clear()
            CType(FoundComboBox2(0), ComboBox).Items.Clear()
            FoundTextBox = Me.Controls.Find(String.Format("TextBox" & i, i), True)
            'Only fill when textboxes are filled
            If CType(FoundTextBox(0), TextBox).Text <> "" Or i > 8 Then
                Dim pRow As DataRow
                For Each pRow In dataSet.Tables("DowntimeVariables").Rows
                    CType(FoundComboBox1(0), ComboBox).Items.Add(pRow("Type"))
                Next
                CType(FoundComboBox1(0), ComboBox).Text = "Wählen"
                CType(FoundComboBox2(0), ComboBox).Text = ""
                CType(FoundComboBox1(0), ComboBox).Enabled = True
                CType(FoundComboBox2(0), ComboBox).Enabled = True
            Else
                CType(FoundComboBox1(0), ComboBox).Text = ""
                CType(FoundComboBox2(0), ComboBox).Text = ""
                CType(FoundComboBox1(0), ComboBox).Enabled = False
                CType(FoundComboBox2(0), ComboBox).Enabled = False
            End If
        Next

    End Sub

    Private Sub GetDataExcelFile()
        'Just pick file for todays year, if new year is started then only the so called standaard values are possible. Then they should just add them to the new file
        Dim FileYear As Integer
        Dim TempDate As Date = Date.Now
        Dim WeekNumber As Integer = DatePart("ww", Date.Now, vbMonday, vbFirstFourDays)

        If WeekNumber = 1 Then
            FileYear = Year(TempDate.AddDays(7))
        ElseIf WeekNumber > 51 Then
            FileYear = Year(TempDate.AddDays(-GetMondayDiff()))
        Else
            FileYear = Year(TempDate)
        End If
        Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)         'Not Year from now, can also be week 53 with few days in next year???

        Dim SQLCMD As New SqlCommand
        Dim queryString As String
        SQLCMD.Connection = DBConnection
        SQLCMD.CommandType = CommandType.Text
        'Delete all values
        queryString = "Delete From dbo.DowntimeVariables"
        SQLCMD.CommandText = queryString
        SQLCMD.ExecuteNonQuery()

        'Check if file exsists
        If Mylog.FileExists(FileName) = False Then
            'Send to logfile
            WriteToLog("GetDataExcelFile: " & FileName & " is not available")
            MsgBox(FileName & " nicht gefunden, Datei sollte verfügbar sein!", , "Fehler")
        Else
            'File is available go on
            'First check if file is opened, then close
            CheckExcelFileOpened()

            oExcelApp = New Excel.Application
            oExcelWorkBook = oExcelApp.Workbooks.Open(FileName)    'set year variable otherwise it will not work for upcoming years (combine weeknumber with year) 
            oWorkSheet = oExcelWorkBook.Worksheets("Entwicklung_Stillstand")

            '----------------------------------------'Read Variables for range

            'Read Values
            For i = 2 To 9      'Technisch Ausfallzeit Tt               'Fields B2 to B9
                queryString = "Insert into dbo.DowntimeVariables (ID, [Type], Cause) Values (" & i & ", 'Technisch Ausfallzeit Tt', '" & oWorkSheet.Cells(i, 3).value & "')"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()
            Next

            For i = 10 To 43      'Organisatorische Ausfalzeit To       'Fiels B10 to B43
                queryString = "Insert into dbo.DowntimeVariables (ID, [Type], Cause) Values (" & i & ",'Organisatorische Ausfalzeit To', '" & oWorkSheet.Cells(i, 3).value & "')"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()
            Next

            oExcelWorkBook.Close(SaveChanges:=False)
            oExcelApp.Quit()
            'DBConnection.Close()
            Marshal.ReleaseComObject(oWorkSheet)
            Marshal.ReleaseComObject(oExcelWorkBook)
            Marshal.ReleaseComObject(oExcelApp)
        End If

        'myTimer.Start()
    End Sub

    Sub CheckExcelFileOpened()
        'Check if excelfile is opened, if so then close

        'Check if file exsists (File of current year)
        Dim WeekNumber As Integer = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)
        Dim FileYear As Integer
        If WeekNumber = 1 Then
            FileYear = Year(Date.Today.AddDays(7))
        ElseIf WeekNumber > 51 Then
            FileYear = Year(Date.Today.AddDays(-GetMondayDiff()))
        Else
            FileYear = Year(Date.Today)
        End If

        Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)
        'Actual check if exsists
        If IO.File.Exists(FileName) Then

            'Check if file is opened
            ''''''''''MsgBox("Already in use")
            Dim xlApp As Microsoft.Office.Interop.Excel.Application
            Dim xlWb As Microsoft.Office.Interop.Excel.Workbook
            Dim FileNameActive As Boolean = False
            Dim FileCount As Int16 = 0
            Dim strWorkBookName As String = Microsoft.VisualBasic.Right(FileName, 36)

            Do
                On Error Resume Next
                xlApp = GetObject(, "Excel.Application")
                If Err.Number <> 0 Then
                    'Excel is NOT open, so the workbook cannot be open
                    Err.Clear()
                    FileNameActive = False
                Else
                    'Excel is open, check if workbook is open
                    FileCount = xlApp.Windows.Count
                    xlWb = xlApp.Workbooks(strWorkBookName)
                    If xlWb Is Nothing Then
                        FileNameActive = False
                    Else
                        'Then close the file, True/false is save file or not
                        xlWb.Close(True)
                        FileNameActive = True
                        xlWb = Nothing
                    End If
                End If
                Exit Do
            Loop

            'If file is opened and no other excel files are open then don't show excelprogram anymore
            If FileNameActive = True And FileCount = 1 Then
                'xlApp.Visible = False
                xlApp.Quit()
            End If
            xlApp = Nothing

        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click
        If System.Windows.Forms.Application.OpenForms().OfType(Of Form2).Any Then
            'Already shown
        Else
            'Show Form2 (Zielwerte)
            Form2.Location = Me.PointToScreen(New System.Drawing.Point(Button1.Right - Form2.Right, Button1.Top - (0.5 * Form2.Bottom)))
            Form2.Show()
            Form2InFront = True
        End If

    End Sub

    Private Sub TabControl1_Selecting(sender As Object, e As TabControlCancelEventArgs) Handles TabControl1.Selecting
        'When tab 2 gets in front then show VNCviewer, otherwise minimize
        Dim iHwnd As IntPtr = FindWindow(vbNullString, ProgVariables(VNCWindowName))
        If TabControl1.SelectedIndex = 2 And (Me.WindowState = FormWindowState.Maximized Or Me.WindowState = FormWindowState.Normal) Then
            If iHwnd = 0 Then
                'VNC Window does not exsists, Start VNC
                Process.Start(ProgVariables(StartVNC))
            Else
                ShowWindow(iHwnd, SW_SHOWNORMAL)
            End If
        Else
            If iHwnd = 0 Then
                'VNC Window does not exsists, Start VNC, don;t start because app is minimized
            Else
                ShowWindow(iHwnd, SW_SHOWMINIMIZED)
            End If
        End If

        If TabControl1.SelectedIndex = 3 Then
            'Fill textboxes and comboboxes
            FillTextBoxes()
            FillComboboxes()
        End If
    End Sub

    Private Sub TabControl1_MouseClick(sender As Object, e As MouseEventArgs) Handles TabControl1.MouseClick
        TabChangeCount = -1
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click
        Button2.Enabled = False
        'Refresh comboboxes
        GetDataExcelFile()
        FillComboboxes()

        Button2.Enabled = True
    End Sub

    Private Sub TextBox_TextChanged(sender As Object, e As EventArgs) Handles TextBox2.TextChanged, TextBox4.TextChanged, TextBox6.TextChanged, TextBox8.TextChanged, TextBox9.TextChanged
        'Check for numeric value (no decimal)
        With CType(sender, TextBox) 'Further on we now which one to implement
            If .Text <> "" Then
                If Not IsNumeric(.Text) Then
                    MsgBox("Wert sollte eine Nummer sein", , "Fehler")
                    .Text = ""
                Else
                    If InStr(1, .Text, ".") Or InStr(1, .Text, ",") Then
                        MsgBox("Nur runde zahlen", , "Fehler")
                        .Text = ""
                    Else
                        'Value is Numeric, Now check value
                        If Int(.Text) > 1440 Then
                            MsgBox("Werte kann nicht höher dann 1440 sein", , "Fehler")
                        Else
                            'Value is ok!
                        End If
                    End If
                End If
            End If
        End With
    End Sub



    Private Sub ComboBox_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ComboBox1.SelectedIndexChanged, ComboBox3.SelectedIndexChanged, ComboBox5.SelectedIndexChanged, ComboBox7.SelectedIndexChanged, ComboBox9.SelectedIndexChanged

        'Determine Combobox and we need to change ComboBox thats one higher
        Dim ComboBoxToFill As ComboBox
        Select Case CType(sender, ComboBox).Name
            Case "ComboBox1"
                ComboBoxToFill = ComboBox2
            Case "ComboBox3"
                ComboBoxToFill = ComboBox4
            Case "ComboBox5"
                ComboBoxToFill = ComboBox6
            Case "ComboBox7"
                ComboBoxToFill = ComboBox8
            Case "ComboBox9"
                ComboBoxToFill = ComboBox10
        End Select

        'Selectedindex, 0 = Technisch, 1 = Organisatorisch
        With sender
            If .SelectedIndex = 0 Then
                'Fill in the "Technische" causes in sender + 1 (One combobox higher)
                ComboBoxToFill.Items.Clear()
                Dim queryString As String = "Select Cause From dbo.DowntimeVariables Where Type = 'Technisch Ausfallzeit Tt'"
                Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
                Dim dataSet As DataSet = New DataSet()

                adapter.Fill(dataSet, "DowntimeVariables")
                Dim pRow As DataRow
                For Each pRow In dataSet.Tables("DowntimeVariables").Rows
                    ComboBoxToFill.Items.Add(pRow("Cause"))
                Next
                ComboBoxToFill.Text = "Wählen"

            ElseIf .SelectedIndex = 1 Then
                'Fill in the "Organisatorisch" causes in sender + 1 (One combobox higher)
                ComboBoxToFill.Items.Clear()
                Dim queryString As String = "Select Cause From dbo.DowntimeVariables Where Type = 'Organisatorische Ausfalzeit To'"
                Dim adapter As SqlDataAdapter = New SqlDataAdapter(queryString, DBConnection)
                Dim dataSet As DataSet = New DataSet()

                adapter.Fill(dataSet, "DowntimeVariables")
                Dim pRow As DataRow
                For Each pRow In dataSet.Tables("DowntimeVariables").Rows
                    ComboBoxToFill.Items.Add(pRow("Cause"))
                Next
                ComboBoxToFill.Text = "Wählen"

            End If
        End With
    End Sub

    Private Sub BtnDelete_Click(sender As Object, e As EventArgs) Handles BtnDelete1.Click, BtnDelete2.Click, BtnDelete3.Click, BtnDelete4.Click
        'Ask if they are sure to delete the data
        If MsgBox("Möchten Sie die Daten löschen?", MessageBoxButtons.YesNo, "Anwendungsmeldung") = DialogResult.No Then
            WriteToLog("BtnDelete_Click: Cancelled")
            'Do nothing
        Else
            WriteToLog("BtnDelete_Click: Delete Data")
            'This deletes the current values from the fields and deletes record from Database
            'Select which button is pressed
            Dim ButtonUsed As Button
            Dim IDToDelete As Integer
            Select Case CType(sender, Button).Name
                Case "BtnDelete1"
                    ButtonUsed = BtnDelete1
                    IDToDelete = DowntimeIDs(0)
                Case "BtnDelete2"
                    ButtonUsed = BtnDelete2
                    IDToDelete = DowntimeIDs(1)
                Case "BtnDelete3"
                    ButtonUsed = BtnDelete3
                    IDToDelete = DowntimeIDs(2)
                Case "BtnDelete4"
                    ButtonUsed = BtnDelete4
                    IDToDelete = DowntimeIDs(3)
            End Select
            ButtonUsed.Enabled = False
            'Delete Record
            Dim SQLCMD As New SqlCommand
            Dim queryString As String
            SQLCMD.Connection = DBConnection
            SQLCMD.CommandType = CommandType.Text
            queryString = "Delete From dbo.Downtimes Where ID = " & IDToDelete
            SQLCMD.CommandText = queryString
            SQLCMD.ExecuteNonQuery()

            'Then refresh text & comboboxes
            FillTextBoxes()
            FillComboboxes()
        End If

    End Sub

    Private Sub Button4_Click(sender As Object, e As EventArgs) Handles Button4.Click
        Button4.Enabled = False
        'If all fields are correct then wright data to database and after that fill in the ExcelFile
        'Check Fields (Startzeit, Zeit, Art and Stillstandsgründe) for active records
        Dim CheckOK As Boolean = False
        Dim FoundComboBox1() As Control
        Dim FoundComboBox2() As Control
        Dim FoundTextBox1() As Control
        Dim FoundTextBox2() As Control
        Dim NumberOfInputs As Integer = 0

        For i = 1 To 7 Step 2
            'Textbox, Textbox, Combobox, Combobox checks
            FoundTextBox1 = Me.Controls.Find(String.Format("TextBox" & i, i), True)
            FoundTextBox2 = Me.Controls.Find(String.Format("TextBox" & (i + 1), i), True)
            FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox" & i, i), True)
            FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox" & (i + 1), i), True)

            If CType(FoundTextBox1(0), TextBox).Text <> "" Then
                If CType(FoundTextBox2(0), TextBox).Text <> "" And (CType(FoundComboBox1(0), ComboBox).Text <> "" And CType(FoundComboBox1(0), ComboBox).Text <> "Wählen") And (CType(FoundComboBox2(0), ComboBox).Text <> "" And CType(FoundComboBox2(0), ComboBox).Text <> "Wählen") Then
                    'All fields are filled
                    CheckOK = True
                Else
                    'Set CheckOK = False And escape loop
                    CheckOK = False
                    Exit For
                End If
                NumberOfInputs += 1
            Else
                'No Record in it CheckOK is true for this
            End If
        Next

        If CheckOK = True Then
            Dim WeekNumber As Integer
            'Write data to database and ExcelFile, Input1 = DowntimeIDs(0), Input2 = DowntimeIDs(1), Input3 = DowntimeIDs(2), Input4 = DowntimeIDs(3)
            For i = 1 To NumberOfInputs
                'Set initial values
                Dim IDToUpdate As Integer
                IDToUpdate = DowntimeIDs(i - 1)
                If i = 1 Then
                    FoundTextBox1 = Me.Controls.Find(String.Format("TextBox1", i), True)
                    FoundTextBox2 = Me.Controls.Find(String.Format("TextBox2", i), True)
                    FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox1", i), True)
                    FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox2", i), True)
                ElseIf i = 2 Then
                    FoundTextBox1 = Me.Controls.Find(String.Format("TextBox3", i), True)
                    FoundTextBox2 = Me.Controls.Find(String.Format("TextBox4", i), True)
                    FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox3", i), True)
                    FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox4", i), True)
                ElseIf i = 3 Then
                    FoundTextBox1 = Me.Controls.Find(String.Format("TextBox5", i), True)
                    FoundTextBox2 = Me.Controls.Find(String.Format("TextBox6", i), True)
                    FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox5", i), True)
                    FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox6", i), True)
                ElseIf i = 4 Then
                    FoundTextBox1 = Me.Controls.Find(String.Format("TextBox7", i), True)
                    FoundTextBox2 = Me.Controls.Find(String.Format("TextBox8", i), True)
                    FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox7", i), True)
                    FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox8", i), True)
                End If

                'Fill in ExcelFile
                Dim Column As Integer
                Dim RowNumber As Integer
                Dim Value As Integer
                Dim FileYear As Integer
                Dim TempDate As Date = CType(FoundTextBox1(0), TextBox).Text
                WeekNumber = DatePart("ww", CType(FoundTextBox1(0), TextBox).Text, vbMonday, vbFirstFourDays)

                If WeekNumber = 1 Then
                    FileYear = Year(TempDate.AddDays(7))
                ElseIf WeekNumber > 51 Then
                    FileYear = Year(TempDate.AddDays(-GetMondayDiff()))
                Else
                    FileYear = Year(TempDate)
                End If
                Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)         'Not Year from now, can also be week 53 with few days in next year???
                'Check if file exsists
                If Mylog.FileExists(FileName) = False Then
                    'Send to logfile
                    WriteToLog("Button4_Click: " & FileName & " is not available")
                    MsgBox(FileName & " nicht gefunden, Datei sollte verfügbar sein!", , "Fehler")
                Else
                    'File is available go on
                    'First check if file is opened, then close
                    CheckExcelFileOpened()

                    oExcelApp = New Excel.Application
                    oExcelWorkBook = oExcelApp.Workbooks.Open(FileName)    'set year variable otherwise it will not work for upcoming years (combine weeknumber with year) 

                    oWorkSheet = oExcelWorkBook.Worksheets("KW" & WeekNumber)
                    'Find correct field (Row and Col)
                    RowNumber = oWorkSheet.Range(ProgVariables(CauseRangeToRead)).Find(CType(FoundComboBox2(0), ComboBox).Text, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Row
                    'First Pick right weekday and use this to select column
                    Dim WeekdayNr As Integer = Weekday(CType(FoundTextBox1(0), TextBox).Text, FirstDayOfWeek.Monday)
                    Dim GermanDay As String
                    Select Case WeekdayNr
                        Case 0  'Sunday
                            GermanDay = "So"
                        Case 1  'Monday
                            GermanDay = "Mo"
                        Case 2  'Tuesday
                            GermanDay = "Di"
                        Case 3  'Wednesday
                            GermanDay = "Mi"
                        Case 4  'Thursday
                            GermanDay = "Do"
                        Case 5  'Friday
                            GermanDay = "Fr"
                        Case 6  'Saturday
                            GermanDay = "Sa"
                    End Select
                    Column = oWorkSheet.Range(ProgVariables(DayRangeToRead)).Find(GermanDay, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Column
                    'MsgBox(Column)
                    Value = oWorkSheet.Cells(RowNumber, Column).Value
                    'MsgBox(Value)
                    oWorkSheet.Cells(RowNumber, Column) = Value + CType(FoundTextBox2(0), TextBox).Text

                    'Update Database
                    Dim SQLCMD As New SqlCommand
                    Dim queryString As String
                    SQLCMD.Connection = DBConnection
                    SQLCMD.CommandType = CommandType.Text
                    queryString = "Update Downtimes Set TotalTimeMinutes = " & CType(FoundTextBox2(0), TextBox).Text & ",  Type = '" & CType(FoundComboBox1(0), ComboBox).Text & "', Cause = '" & CType(FoundComboBox2(0), ComboBox).Text & "' Where ID = " & IDToUpdate
                    SQLCMD.CommandText = queryString
                    SQLCMD.ExecuteNonQuery()

                    If oExcelWorkBook.Saved = False Then
                        oExcelWorkBook.Save()
                    End If
                    oExcelWorkBook.Close()
                    oExcelApp.Quit()
                    Marshal.ReleaseComObject(oWorkSheet)
                    Marshal.ReleaseComObject(oExcelWorkBook)
                    Marshal.ReleaseComObject(oExcelApp)
                End If

            Next
            FillTextBoxes()
            FillComboboxes()
        Else
            'Do nothing Check not OK
            MsgBox("Eingabe nicht korrekt!", , "Fehler")
        End If

        Button4.Enabled = True
    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click
        Button3.Enabled = False
        'If all fields are correct then wright data to database and after that fill in the ExcelFile
        'Check Fields (Startzeit, Zeit, Art and Stillstandsgründe) for active records
        Dim CheckOK As Boolean = False
        Dim FoundComboBox1() As Control
        Dim FoundComboBox2() As Control
        Dim FoundDateTimePicker() As Control
        Dim FoundTextBox() As Control
        Dim NumberOfInputs As Integer = 0


        'Textbox, Textbox, Combobox, Combobox checks
        FoundDateTimePicker = Me.Controls.Find(String.Format("DateTimePicker1", 1), True)
        FoundTextBox = Me.Controls.Find(String.Format("TextBox9", 9), True)
        FoundComboBox1 = Me.Controls.Find(String.Format("ComboBox9", 9), True)
        FoundComboBox2 = Me.Controls.Find(String.Format("ComboBox10", 10), True)

        If CType(FoundDateTimePicker(0), DateTimePicker).Text <> "" Then
            If CType(FoundTextBox(0), TextBox).Text <> "" And (CType(FoundComboBox1(0), ComboBox).Text <> "" And CType(FoundComboBox1(0), ComboBox).Text <> "Wählen") And (CType(FoundComboBox2(0), ComboBox).Text <> "" And CType(FoundComboBox2(0), ComboBox).Text <> "Wählen") Then
                'All fields are filled
                CheckOK = True
            Else
                'Set CheckOK = False And escape loop
                CheckOK = False
            End If
            NumberOfInputs += 1
        Else
            'No Record in it CheckOK is true for this
        End If


        If CheckOK = True Then
            Dim WeekNumber As Integer
            'Write data to database and ExcelFile, Input1 = DowntimeIDs(0), Input2 = DowntimeIDs(1), Input3 = DowntimeIDs(2), Input4 = DowntimeIDs(3)
            'Fill in ExcelFile
            Dim Column As Integer
            Dim RowNumber As Integer
            Dim Value As Integer
            Dim FileYear As Integer
            Dim TempDate As Date = CType(FoundDateTimePicker(0), DateTimePicker).Text
            WeekNumber = DatePart("ww", CType(FoundDateTimePicker(0), DateTimePicker).Text, vbMonday, vbFirstFourDays)

            If WeekNumber = 1 Then
                FileYear = Year(TempDate.AddDays(7))
            ElseIf WeekNumber > 51 Then
                FileYear = Year(TempDate.AddDays(-GetMondayDiff()))
            Else
                FileYear = Year(TempDate)
            End If
            Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)         'Not Year from now, can also be week 53 with few days in next year
            'Check if file exsists
            If Mylog.FileExists(FileName) = False Then
                'Send to logfile
                WriteToLog("Button3_Click: " & FileName & " is not available")
                MsgBox(FileName & " nicht gefunden, Datei sollte verfügbar sein!", , "Fehler")
            Else
                'File is available go on
                'First check if file is opened, then close
                CheckExcelFileOpened()

                oExcelApp = New Excel.Application
                oExcelWorkBook = oExcelApp.Workbooks.Open(FileName)    'set year variable otherwise it will not work for upcoming years (combine weeknumber with year) 

                oWorkSheet = oExcelWorkBook.Worksheets("KW" & WeekNumber)
                'Find correct field (Row and Col)
                RowNumber = oWorkSheet.Range(ProgVariables(CauseRangeToRead)).Find(CType(FoundComboBox2(0), ComboBox).Text, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Row
                'First Pick right weekday and use this to select dolumn
                Dim WeekdayNr As Integer = Weekday(CType(FoundDateTimePicker(0), DateTimePicker).Text, FirstDayOfWeek.Monday)
                Dim GermanDay As String
                Select Case WeekdayNr
                    Case 0  'Sunday
                        GermanDay = "So"
                    Case 1  'Monday
                        GermanDay = "Mo"
                    Case 2  'Tuesday
                        GermanDay = "Di"
                    Case 3  'Wednesday
                        GermanDay = "Mi"
                    Case 4  'Thursday
                        GermanDay = "Do"
                    Case 5  'Friday
                        GermanDay = "Fr"
                    Case 6  'Saturday
                        GermanDay = "Sa"
                End Select
                Column = oWorkSheet.Range(ProgVariables(DayRangeToRead)).Find(GermanDay, , Excel.XlFindLookIn.xlValues, Excel.XlLookAt.xlWhole, Excel.XlSearchOrder.xlByRows, Excel.XlSearchDirection.xlNext, False).Column
                'MsgBox(Column)
                Value = oWorkSheet.Cells(RowNumber, Column).Value
                'MsgBox(Value)
                oWorkSheet.Cells(RowNumber, Column) = Value + CType(FoundTextBox(0), TextBox).Text

                'Update Database
                Dim SQLCMD As New SqlCommand
                Dim queryString As String
                SQLCMD.Connection = DBConnection
                SQLCMD.CommandType = CommandType.Text
                queryString = "Insert into dbo.Downtimes (Date, TotalTimeMinutes, [Type], Cause, WeekNumber, Undifined) Values ('" & CType(FoundDateTimePicker(0), DateTimePicker).Text & "', " & CType(FoundTextBox(0), TextBox).Text & ", '" & CType(FoundComboBox1(0), ComboBox).Text & "', '" & CType(FoundComboBox2(0), ComboBox).Text & "', " & WeekNumber & ", 'False')"
                SQLCMD.CommandText = queryString
                SQLCMD.ExecuteNonQuery()

                If oExcelWorkBook.Saved = False Then
                    oExcelWorkBook.Save()
                End If
                oExcelWorkBook.Close()
                oExcelApp.Quit()
                Marshal.ReleaseComObject(oWorkSheet)
                Marshal.ReleaseComObject(oExcelWorkBook)
                Marshal.ReleaseComObject(oExcelApp)
            End If
            TextBox9.Text = ""
            FillComboboxes()
        Else
            'Do nothing Check not OK
            MsgBox("Eingabe nicht korrekt!", , "Fehler")
        End If

        Button3.Enabled = True
    End Sub

    Private Sub ComboBox_KeyPress(sender As Object, e As KeyPressEventArgs) Handles ComboBox1.KeyPress, ComboBox10.KeyPress, ComboBox9.KeyPress, ComboBox8.KeyPress, ComboBox7.KeyPress, ComboBox6.KeyPress, ComboBox5.KeyPress, ComboBox4.KeyPress, ComboBox3.KeyPress, ComboBox2.KeyPress
        e.Handled = True
    End Sub


    Private Sub Openfile_Click(sender As Object, e As EventArgs) Handles Openfile.Click
        'Msgbox that says "Entering Data into the KW sheets will not be available for application!"
        MsgBox("Eingegebene werte in Excel werden nicht in der anwendung angezeigt, nur die gründe konnte sie aktualisieren.", , "Anwendungsmeldung")

        'Check if file exsists (File of current year)
        Dim WeekNumber As Integer = DatePart("ww", Date.Today, vbMonday, vbFirstFourDays)
        Dim FileYear As Integer
        If WeekNumber = 1 Then
            FileYear = Year(Date.Today.AddDays(7))
        ElseIf WeekNumber > 51 Then
            FileYear = Year(Date.Today.AddDays(-GetMondayDiff()))
        Else
            FileYear = Year(Date.Today)
        End If

        Dim FileName As String = ProgVariables(DiagramPathName) & FileYear & ProgVariables(DiagramlFileExtension)

        If IO.File.Exists(FileName) Then
            'Open file
            Process.Start(FileName)
        Else
            WriteToLog("Openfile_Click: " & FileName & "not available")
            MsgBox("Datei existiert nicht", , "Fehler")
        End If

    End Sub

    Private Sub BtnConnect_Click(sender As Object, e As EventArgs) Handles BtnConnect.Click
        If ConnectedState = False Then
            'Try to connect
            If ConnectToPLC() Then
                'Connected
                ConnectedState = True
                WritePLCValue(ProgVariables(OUT_IPC_Alive_Bit), "True", PLC_Alive_Bit)
                WriteToLog("BtnConnect_Click: Succesfully connected")
                BtnConnect.Visible = False
            Else
                'Set ConnectedState to False
                MsgBox("Konnte verbindung nicht herstellen", , "Fehler")
                ConnectedState = False
                WriteToLog("BtnConnect_Click: Not connected")
            End If
        Else
            'Already connected
            WriteToLog("BtnConnect_Click: Already connected")
        End If
    End Sub
End Class
